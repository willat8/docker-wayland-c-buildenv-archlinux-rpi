FROM scratch

ADD ArchLinuxARM-2016.08-rpi-rootfs.tar.gz /

COPY qemu-arm-static /usr/bin/qemu-arm-static

# Install build tools
RUN pacman -Sy gcc automake autoconf libtool make pkgconfig git --noconfirm

# Install the weston build dependencies
RUN pacman -S weston wayland-protocols zlib --noconfirm

RUN git -C /usr/src clone https://github.com/wayland-project/weston.git

WORKDIR /usr/src/weston

RUN ./autogen.sh \
 && make -j9

